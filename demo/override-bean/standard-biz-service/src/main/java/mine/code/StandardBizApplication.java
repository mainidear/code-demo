package mine.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StandardBizApplication {

    public static void main(String[] args) {
        SpringApplication.run(StandardBizApplication.class, args);
    }

}
