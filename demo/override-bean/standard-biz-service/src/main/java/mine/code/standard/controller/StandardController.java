package mine.code.standard.controller;

import mine.code.standard.service.inter.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:13
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@RestController
public class StandardController {
    @Autowired
    private UserService userService;

    @RequestMapping("hello")
    public Object hello() {
        return "hello: " + new Date().toString() + " " + userService.hello();
    }
}
