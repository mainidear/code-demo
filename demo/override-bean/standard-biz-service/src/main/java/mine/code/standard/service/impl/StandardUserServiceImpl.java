package mine.code.standard.service.impl;

import mine.code.standard.service.inter.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:27
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Component
public class StandardUserServiceImpl implements UserService {
    /**
     * 日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardUserServiceImpl.class);

    @Override
    public String getUserNameById(int id) {
        LOGGER.info("getUserNameById标准实现");
        return "StandardName_" + id;
    }


    @Override
    public String hello() {
        return "StandardUserServiceImpl";
    }
}