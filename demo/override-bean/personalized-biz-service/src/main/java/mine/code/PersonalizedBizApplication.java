package mine.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalizedBizApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalizedBizApplication.class, args);
    }

}
