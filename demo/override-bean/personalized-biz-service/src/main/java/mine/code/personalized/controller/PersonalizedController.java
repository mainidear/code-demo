package mine.code.personalized.controller;

import mine.code.standard.service.inter.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:14
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@RestController
public class PersonalizedController {
    @Autowired
    private UserService userService;


    @RequestMapping("hi")
    public Object hello() {
        return "hi: " + new Date().toString() + " " + userService.hello();
    }
}