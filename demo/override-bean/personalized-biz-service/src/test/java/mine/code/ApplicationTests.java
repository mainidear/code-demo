package mine.code;

import mine.code.standard.service.inter.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:29
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@SpringBootTest(classes = PersonalizedBizApplication.class)
public class ApplicationTests {
    @Autowired
    private UserService userInter;

    @Test
    public void testLogin() {
        String userName = userInter.getUserNameById(1);
        System.out.println(userName);
    }
}