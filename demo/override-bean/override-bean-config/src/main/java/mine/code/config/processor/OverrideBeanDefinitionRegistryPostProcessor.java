package mine.code.config.processor;

import mine.code.config.annotation.OverrideBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 重写bean注册处理
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:02
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Component
public class OverrideBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor, EnvironmentAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(OverrideBeanDefinitionRegistryPostProcessor.class);
    /**
     * 业务bean实现类前缀（用于过滤不必要的bean处理）
     */
    public static final String KEY_BIZ_BEAN_CLASS_PREFIX = "mine.biz.bean.class.prefix";
    /**
     * 业务bean实现类前缀默认值
     */
    private static final String DEFAULT_BIZ_BEAN_CLASS_PREFIX = "mine.code";
    /**
     * 业务bean实现类前缀（包名）
     */
    private String bizBeanClassPrefix;


    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        // do nothing
    }

    /**
     * 核心逻辑为用OverrideBean替换注册被继承的bean
     */
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        // 所有业务bean的map，key为bean实现类的类名，value为bean名称
        HashMap<String, String> bizBeanImplClassName2BeanNameMap = new HashMap<>();

        // 所有重写的业务bean（含OverrideBean注解且继承了另一个bean实现类）的map，
        // key为overrideBean的实现类的父类的类名，value为bean名称
        HashMap<String, String> overrideBizBeanImplParentClassName2BeanNameMap = new HashMap<>();

        // 按bean名称遍历所有的bean定义，以初始化className2BeanNameMap和overrideBeanClassName2BeanNameMap
        for (String beanName : registry.getBeanDefinitionNames()) {
            // 获取bean定义
            BeanDefinition beanDefinition = registry.getBeanDefinition(beanName);

            // bean实现类名
            String beanImplClassName = beanDefinition.getBeanClassName();

            // bean对应的实现类名不存在 或 不是自己指定的某个服务包目录下的bean（所有的业务服务bean都应在此包目录下）
            if (beanImplClassName == null || !beanImplClassName.startsWith(bizBeanClassPrefix)) {
                continue;
            }

            // 将所有的业务bean定义放入map
            bizBeanImplClassName2BeanNameMap.put(beanImplClassName, beanName);

            try {
                // 加载bean实现类对应的类
                Class<?> beanImplClass = Class.forName(beanDefinition.getBeanClassName());

                // 判断bean类上有没有【OverrideBean】重写bean注解
                OverrideBean overrideBean = beanImplClass.getAnnotation(OverrideBean.class);
                if (overrideBean != null && beanImplClass.getGenericSuperclass() != Object.class) {
                    // 有注解，放到map，key为所继承的父类的类名（即被覆盖的bean的实现类）
                    overrideBizBeanImplParentClassName2BeanNameMap.put(beanImplClass.getGenericSuperclass().getTypeName(), beanName);
                }
            } catch (Exception e) {
                LOGGER.error("处理OverrideBean异常", e);
            }
        }

        // 遍历所有重写bean，并替换注册被重写的bean（重写的bean以其父类bean的名称注册代之）
        for (Map.Entry<String, String> overrideBeanEntry : overrideBizBeanImplParentClassName2BeanNameMap.entrySet()) {
            // bean实现类的类名（被重写bean的父类，即被继承的bean的实现类）
            String originalBeanImplClassName = overrideBeanEntry.getKey();

            // bean名称
            String overrideBeanName = overrideBeanEntry.getValue();

            // 从所有beanMap中取出被继承父类对应的（被重写bean类型）的bean名称
            String originalBeanName = bizBeanImplClassName2BeanNameMap.get(originalBeanImplClassName);
            BeanDefinition originalBeanDefinition = registry.getBeanDefinition(originalBeanName);

            // 移除掉原bean定义
            registry.removeBeanDefinition(originalBeanName);

            // 将重写bean以原bean名称注册
            BeanDefinition overrideBeanDefinition = registry.getBeanDefinition(overrideBeanName);
            registry.registerBeanDefinition(originalBeanName, overrideBeanDefinition);

            // 移除重写bean注册
            registry.removeBeanDefinition(overrideBeanName);


            LOGGER.warn("OverrideBean: 将bean的实现类由[{}]替换为[{}]",
                    originalBeanDefinition.getBeanClassName(), overrideBeanDefinition.getBeanClassName());
        }

        // 清空map
        bizBeanImplClassName2BeanNameMap.clear();
        overrideBizBeanImplParentClassName2BeanNameMap.clear();
    }

    /**
     * 初始化bean类前缀配置
     */
    @Override
    public void setEnvironment(Environment environment) {
        bizBeanClassPrefix = environment.getProperty(KEY_BIZ_BEAN_CLASS_PREFIX, DEFAULT_BIZ_BEAN_CLASS_PREFIX);
    }
}