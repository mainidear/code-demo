package code.demo.test;

import code.demo.ServiceStarter;
import code.demo.dynamic.proxy.mapper.AccountDao;
import code.demo.dynamic.proxy.mapper.UserDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:29
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@SpringBootTest(classes = ServiceStarter.class)
public class MapperTest {
    @Autowired
    private UserDao userDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    public void testLogin() {
        String name = userDao.getNameById(1);
        System.err.println(name);

        String address = userDao.getAddressById(2);
        System.err.println(address);

        System.err.println(accountDao.getAccountNameById(3));
    }
}