package code.demo.dynamic.proxy.mapper.binding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * mapper类代理处理类
 * MapperProxy.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 16:34
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
public class MapperProxy implements InvocationHandler {
    /**
     * 日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MapperProxy.class);

    /**
     * applicationContext，用于获取业务处理时获取bean
     */
    private ApplicationContext applicationContext;

    public MapperProxy(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * 代理方法处理逻辑
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LOGGER.info("方法[{}]代理实现", method.getName());

        if (Object.class.equals(method.getDeclaringClass())) {
            // 是Object类的方法，调用原有实现
            return method.invoke(this, args);
        }

        // 自定义的实现逻辑，任由发挥

        return "代理处理结果：" + method.getName();
    }
}