package code.demo.dynamic.proxy.mapper.binding;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Proxy;

/**
 * MapperFactoryBean.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 16:49
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
public class MapperFactoryBean<T> implements FactoryBean<T> {
    /**
     * 接口类
     */
    private Class<T> mapperInterClass;
    /**
     * applicationContext
     */
    private ApplicationContext applicationContext;

    public MapperFactoryBean(Class<T> mapperInterClass, ApplicationContext applicationContext) {
        this.mapperInterClass = mapperInterClass;
        this.applicationContext = applicationContext;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getObject() {
        return (T) Proxy.newProxyInstance(
                this.getClass().getClassLoader(),
                new Class[]{mapperInterClass},
                new MapperProxy(applicationContext));
    }

    @Override
    public Class<?> getObjectType() {
        return mapperInterClass;
    }

    /**
     * Is the object managed by this factory a singleton? That is,
     * will {@link #getObject()} always return the same object
     * (a reference that can be cached)?
     */
    @Override
    public boolean isSingleton() {
        return true;
    }
}