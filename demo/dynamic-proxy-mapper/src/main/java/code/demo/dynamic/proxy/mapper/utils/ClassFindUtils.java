package code.demo.dynamic.proxy.mapper.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

/**
 * ClassFindUtils.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 14:40
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
public class ClassFindUtils {
    /**
     * 日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassFindUtils.class);
    /**
     * （类文件）资源文件通配符
     */
    private static final String RESOURCE_PATTERN = "/**/*.class";


    /**
     * 根据 包路径+注解 查询类
     *
     * @param annotationClass 注解类
     * @param <A>             注解类泛型
     * @param basePackages    包路径（例：com.example.demo）
     * @return 类集合
     * 参考实现：ClassPathScanningCandidateComponentProvider#scanCandidateComponents
     */
    public static <A extends Annotation> Set<Class<?>> findClass(Class<A> annotationClass,
                                                                 String... basePackages) {
        Set<Class<?>> classSet = new HashSet<>();

        for (String basePackage : basePackages) {
            classSet.addAll(findClass(annotationClass, basePackage));
        }

        return classSet;
    }

    /**
     * 根据 包路径+注解 查询类
     *
     * @param annotationClass 注解类
     * @param <A>             注解类泛型
     * @param basePackage     包路径（例：com.example.demo）
     * @return 类集合
     * ClassPathScanningCandidateComponentProvider#scanCandidateComponents
     */
    public static <A extends Annotation> Set<Class<?>> findClass(Class<A> annotationClass,
                                                                 String basePackage) {
        Set<Class<?>> classSet = new HashSet<>();

        try {
            // spring工具类，可以获取指定路径下的全部类
            ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

            String pattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                    ClassUtils.convertClassNameToResourcePath(basePackage) + RESOURCE_PATTERN;

            Resource[] resources = resourcePatternResolver.getResources(pattern);

            // MetadataReader 的工厂类
            MetadataReaderFactory readerFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
            for (Resource resource : resources) {
                // 用于读取类信息
                MetadataReader reader = readerFactory.getMetadataReader(resource);
                // 扫描到的class
                String classname = reader.getClassMetadata().getClassName();
                Class<?> clazz = Class.forName(classname);
                // 判断是否有指定主解
                A annotation = clazz.getAnnotation(annotationClass);
                if (annotation != null) {
                    classSet.add(clazz);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.error("扫描指定包[{}]下含注解[{}]的类异常", basePackage, annotationClass.getName());
            LOGGER.error("异常信息", e);
        }

        return classSet;
    }
}