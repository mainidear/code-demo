package code.demo.dynamic.proxy.mapper;

import code.demo.dynamic.proxy.mapper.annotation.Mapper;

/**
 * UserDao.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 16:53
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Mapper
public interface UserDao {
    String getNameById(int id);

    String getAddressById(int id);
}
